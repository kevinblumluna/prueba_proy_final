#include "../include/vida.h"
#include "../include/util.h"

bloque_t *bloqueU = NULL;
sem_t *principal2 = NULL;
sem_t *worker2 = NULL;
sem_t mutex2 = {0};
int *formas;

void buildBloque(bloque_t *bloque){
	int posX[] = {1,1,2,2,1,1,2,2,3,3,1,2,2,3,3,4,1,1,2,2,3,3,4,1,1,2,2,3,1,2,2,3};
	int posY[] = {1,2,1,2,2,3,1,4,2,3,2,1,3,1,3,2,2,3,1,4,2,4,3,1,2,1,3,2,2,1,3,2};
	int dimensionX[] = {4,5,6,6,5,5};
	int dimensionY[] = {4,6,5,6,5,5};
	int number[] = {4,6,6,7,5,4};
	int block = 0;
	int pos = 0;

	while(block<6){
		(bloque[block]).bloque = (char **) malloc(dimensionX[block]*sizeof(char *));
		(bloque[block]).dimensionX = dimensionX[block];
		(bloque[block]).dimensionY = dimensionY[block];
		(bloque[block]).id = block;
		for(int i = 0;i<dimensionX[block];i++)
			(bloque[block]).bloque[i] = (char *) calloc(dimensionY[block],sizeof(char));
		while(number[block] != 0){
			(bloque[block]).bloque[posX[pos]][posY[pos]] = 1;
			pos++;
			number[block]--;
		}
		block++;
		
	}
}

int checkPointForm(int x, int y,char **matriz, bloque_t *bloque4){
	int sum = 0;
	for(int i= 0; i<bloque4->dimensionX;i++){
		for(int j=0; j<bloque4->dimensionY;j++){
			if(bloque4->bloque[i][j] == matriz[x+i][y+j]){
				sum++;		
			}
		}
	}
	int controlador = bloque4->dimensionX*bloque4->dimensionY;
	return (sum==controlador) ? 1:0;
}

void* hiloCheckForm(void *varg){
	bloque_t *bloque2 = (bloque_t*) varg;
	extern juego_de_vida *juego;
	int i,j,form = 0;
	while(1){
		sem_wait(&worker2[bloque2->id]);
		i = 0;	
		form = 0;	
		while(i<juego->filas){
			j=0;
			int tempX = i+bloque2->dimensionX-1;
			if(tempX >= juego->filas){
				i= juego->filas;
			}else {
				while(j<juego->columnas){
					int tempY = j+bloque2->dimensionY-1;
					if(tempY >= juego->columnas){
						j = juego->columnas;				
					}else {
						int control = checkPointForm(i,j,juego->tablero,bloque2);
						if(control == 1){
							form++;
						}
						j++;
					}
				}
				i++;
			}
		}
		sem_wait(&mutex2);
		formas[bloque2->id] += form;
		sem_post(&mutex2);
		sem_post(&principal2[bloque2->id]);
	}
}

void mostrarFormas(FILE *fd){

	fprintf(fd,"\nFormas detectadas:\n\n");
	fprintf(fd,"Bloque: %d\n", formas[0]);
	fprintf(fd,"Colmena Horizontal: %d\n", formas[1]);
	fprintf(fd,"Colmena Vertical: %d\n", formas[2]);
	fprintf(fd,"Hogaza: %d\n", formas[3]);
	fprintf(fd,"Bote: %d\n", formas[4]);
	fprintf(fd,"Tina: %d\n", formas[5]);
}



