
#ifndef VIDA_H
#define VIDA_H

#include <pthread.h>
#include <semaphore.h>

typedef struct vida{
	char **tablero;	
	long filas;
	long columnas;
	long tiempo_sleep;
	long generaciones;	
} juego_de_vida;


typedef struct HiloData{
	int id;
	int cantidadhilos;
}hilo_t;

typedef struct estadistica_t{
	int vivascontador;
	int muertocontador;
	int vivo;
	int muerto;
	int mostrar;
	float vivaspromedio;
	float muertaspromedio;
	int acumuladorvivos;
	int acumuladormuertos;
	int genControl;
}estadistica_t;

typedef struct bloque{
	char **bloque;
	int dimensionX;
	int dimensionY;
	int id;
}bloque_t;


#endif 
