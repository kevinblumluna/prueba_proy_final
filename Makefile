bin/vida: obj/main.o obj/util.o obj/bloque.o
	gcc -g -Wall -fsanitize=address,undefined obj/bloque.o obj/util.o obj/main.o -lpthread -o bin/vida
obj/bloque.o: src/bloque.c
	gcc -g -c -I include/ src/bloque.c -o obj/bloque.o
obj/util.o: src/util.c
	gcc -g -c -I include/ src/util.c -o obj/util.o
obj/main.o: src/main.c
	gcc -g -c -I include/ src/main.c -o obj/main.o
.PHONY: clean
clean:
	rm -rf obj bin
	mkdir  obj bin
