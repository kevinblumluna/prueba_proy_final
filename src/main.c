#include "../include/vida.h"
#include "../include/util.h"


pthread_t *tid;
pthread_t *tid2;

int main(int argc, char* argv[]) {
	//umask(0);
	char control = 0;

	if(argc<13 || argc>14){
		printf("Argumentos invalidos\n");
		return -1;
	}

	extern juego_de_vida *juego;
	extern estadistica_t *estad;
	extern char mostrar;
	juego = (juego_de_vida *) malloc(sizeof(juego_de_vida));
	estad = (estadistica_t *) calloc(1,sizeof(estadistica_t));
	int opt;
	int inicio=0;
	
	int nHilos;
	int nHilos2 = 6;

	while((opt = getopt(argc, argv, ":f:c:g:s:i:n:m")) != -1){

		switch(opt){

			case 'f':
			    	juego->filas=atoi(optarg);
				if(juego->filas<=0){
					printf("Ingrese un numero valido\n");
					return -1;
				}
				break;
			case 'c':
			    	juego->columnas=atoi(optarg);
				if(juego->columnas<=0){
					printf("Ingrese un numero valido\n");
				return -1;
				}
				break;
			case 'g':
			    	juego->generaciones=atoi(optarg);
				break;
			case 's':
			    	juego->tiempo_sleep=atoi(optarg);
				if(juego->tiempo_sleep<0){
					printf("Ingrese  un numero valido\n");
				return -1;
				}
				break;
			case 'i':
			    	inicio=atoi(optarg);
				if(inicio<=0){
					printf("Ingrese un numero valido\n");
					return -1;
				}
				break;
			
			case 'n':
			    	nHilos=atoi(optarg);
				if(nHilos<=0){
					printf("Ingrese un numero valido\n");
					return -1;
				}
				break;

			case 'm':
			    	mostrar=1;
				break;
			default:
				printf("Argumentos incorrectos\n");
				return -1;
		}
	}


	if(((juego->filas*juego->columnas)-inicio) < 0){//validacion que la celulas no sean mas que el tamano de la matriz
		printf("Ingrese un numero valido\n");
		return -1;
	}
	

  	juego->tablero = (char**)malloc(sizeof(char*)*(juego->filas));//filas
  	if(juego->tablero == NULL){
		return -1;
  	}

	for (int i=0; i < juego->filas ; i++){
    		juego->tablero[i] = (char*)malloc(sizeof(char)*juego->columnas);//columnas
    		if(juego->tablero[i] == NULL)
			return -1;
  		
	}

	if(juego->generaciones > 0)
		control = 1;

	extern bloque_t *bloqueU;
	bloqueU = (bloque_t *) malloc(6*sizeof(bloque_t));
	buildBloque(bloqueU);
	extern sem_t *principal2;
	extern sem_t *worker2;
	extern sem_t mutex2;	
	extern int *formas;
	extern sem_t mutex;
	extern sem_t *principal;
	extern sem_t *worker;
	formas = (int *) calloc(6,sizeof(int));
	principal = (sem_t *) malloc(nHilos*sizeof(sem_t));
	worker = (sem_t *) malloc(nHilos*sizeof(sem_t));
	principal2 = (sem_t *) malloc(nHilos2*sizeof(sem_t));
	worker2 = (sem_t *) malloc(nHilos2*sizeof(sem_t));
	sem_init(&mutex,0,1);
	sem_init(&mutex2,0,1);
	
	for(int i = 0; i < nHilos; i++){
		sem_init(&principal[i], 0, 0);
		sem_init(&worker[i],0, 0);
	}

	for(int i = 0; i < nHilos2; i++){
		sem_init(&principal2[i], 0, 0);
		sem_init(&worker2[i],0, 0);
	}

	tid = (pthread_t *) malloc(sizeof(pthread_t)*nHilos);
	tid2 = (pthread_t *) malloc(sizeof(pthread_t)*nHilos2);
	extern hilo_t *datos;
	datos = (hilo_t *) malloc(sizeof(hilo_t)*nHilos);

	for(int i=0; i<nHilos;i++){
		(datos + i)->id = i;
		(datos + i)->cantidadhilos = nHilos;
		pthread_create(&tid[i],NULL,trabajador,(void *) &datos[i]);
	}

	for(int j=0; j<nHilos2;j++){
		pthread_create(&tid2[j],NULL,hiloCheckForm,(void *) &bloqueU[j]);
	}


	extern char **matriznueva;
	matriznueva = (char **)malloc(sizeof(char*)*(juego->filas));
	for(int i=0;i<juego->filas;i++)
		matriznueva[i]=(char *)malloc(sizeof(char )*juego->columnas);
 
  	int muertas=0;
  	muertas=(juego->filas*juego->columnas)-inicio;

  	llenar_matriz_azar(juego->tablero, juego->filas,juego->columnas,inicio);

  	if(mostrar==1)
 		 dibujar_grilla(juego->tablero, juego->filas, juego->columnas,stdout);

	estad->vivo = estad->vivascontador = inicio;
	//printf("main celulas Iniciadas-> %0.2f\n",(float)c_ini );
	//printf("main celulas muertas-> %0.2f\n",(float)muertas );

	estad->acumuladorvivos += inicio;

  	mostrarEstadistica(stdout);

  	nueva_generacion(control, nHilos, nHilos2);
       
        //int* fd = open("resultado.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
//	mostrarFormas(stdout);
	
  	for(int i=0;i<juego->filas;i++){
		free(juego->tablero[i]);
		juego->tablero[i]=NULL;
	
  	}
  	free(juego->tablero);
  	juego->tablero=NULL;

	for(int i=0;i<juego->filas;i++){
		free(matriznueva[i]);
		matriznueva[i] = NULL;
	}

	free(matriznueva);
	matriznueva = NULL;

  	free(juego);
	free(estad);
  	return 0;

}
