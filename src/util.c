#include "../include/vida.h"
#include "../include/util.h"


int acumuladorvivos=0,acumuladormuertos=0;
int cmuertas=0,cvivas=0,vecinos_vivos=0;
int cont=0;
int aliveA = 0;
int vivo2 = 0;
char **matriz=NULL;
char mostrar = 0;

hilo_t * datos = NULL;
juego_de_vida *juego = NULL;
estadistica_t *estad = NULL;
char **matriznueva = NULL;

sem_t *principal = NULL;	//empiezan en 1
sem_t *worker = NULL;
sem_t mutex = {0};

extern sem_t *principal2;
extern sem_t *worker2;


void mostrarEstadistica(FILE *fd){

	fprintf(fd,"Generación %d\n", estad->genControl);
	fprintf(fd,"Número de células que nacieron en esta generación: %d\n", estad->vivo);
	fprintf(fd,"Número de células que murieron en esta generación: %d\n", estad->muerto);
	fprintf(fd,"Número de células que han nacido desde generación 0: %d\n", estad->vivascontador);
	fprintf(fd,"Número de células que han muerto desde generación 0: %d\n", estad->muertocontador);
	fprintf(fd,"Promedio de células que viven por generación: %0.2f\n", estad->vivaspromedio);
	fprintf(fd,"Promedio de células que muerto por generación: %0.2f\n", estad->muertaspromedio);
}

void dibujar_grilla(char **matriz, int fil, int col,FILE *fd){
	printf("\e[1;1H\e[2J");
	char *linea = malloc(col + 1);//Char nulo al final
	if(linea==NULL){
		return;
	}
	for(int i = 0; i < fil; i++){
		memset(linea, ' ', col+1);
		linea[col] = 0;
		for(int j = 0; j < col; j++){
			if(matriz[i][j] == 0){
				linea[j] = ' ';
			}
			else if(matriz[i][j] == 1){
				linea[j] = 'x';
			}
		}
		fprintf(fd,"%s", linea);
		fprintf(fd,"\n");
		fflush(stdout);
	}
	free(linea);
}

int encontrar_vecino(juego_de_vida *juego, int posX, int posY){

	int vecino = 0;
	for(int i= posX-1; i<posX+2; i++){
		if(i>=0 && i<juego->filas){
			for(int j = posY-1; j<posY+2; j++){
				if(j>=0 && j<juego->columnas){
					if(juego->tablero[i][j] == 1){
						vecino++;						
						if(i == posX && j == posY) vecino--;
					}
				}
			}
		}
	}
	return vecino;
}


void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad){
	for(int i =0; i<fil; i++){
		memset(grilla[i], 0, col);
	}
	srand(time(NULL));
	for(int i = 0; i < cantidad; i++){
		long rnd_fil = rand() % fil;
		long rnd_col = rand() % col;

		if(grilla[rnd_fil][rnd_col] == 1){
			i--;
		}
		else{
			grilla[rnd_fil][rnd_col] = 1;
		}
	}
	cvivas=cantidad;
	cmuertas=(fil*col)-cantidad;

}


void *trabajador(void *args){
	hilo_t *data = (hilo_t *) args;
	unsigned long hiloID = data->id;
	unsigned long salto = 1;
	
	while(1){
		  
		sem_wait(&worker[data->id]);		
		
		int thisGenBorn = 0;
		int thisGenDead = 0;
		int thisGenAlive = 0;
		hiloID = data->id;
		salto= 1;	
		int vecinos_vivos;
		while(hiloID<juego->filas){
			for(unsigned long column=0; column < juego->columnas; column++){

				if(juego->tablero[hiloID][column]==1)
					thisGenAlive++;

				vecinos_vivos= encontrar_vecino(juego, hiloID, column);
				
				if((juego->tablero[hiloID][column]==0)&&(vecinos_vivos==3)){
					matriznueva[hiloID][column]=1;
					thisGenBorn++;
				}
				else if((juego->tablero[hiloID][column]==1)&&(vecinos_vivos<2)){
					matriznueva[hiloID][column]=0;
					thisGenDead++;
				}
				else if((juego->tablero[hiloID][column]==1)&&(vecinos_vivos>3)){
					matriznueva[hiloID][column]=0;
					thisGenDead++;
				}
				else if((juego->tablero[hiloID][column]==1)&&(vecinos_vivos==2||vecinos_vivos==3)){
					matriznueva[hiloID][column]=1;
				}
					
			}
			hiloID = (data->id) + (salto*data->cantidadhilos);
			salto++;
		}

		sem_wait(&mutex);
		aliveA += thisGenAlive;
		estad->vivo += thisGenBorn;
		estad->muerto += thisGenDead;		
		sem_post(&mutex);

		sem_post(&principal[data->id]);
	}
}



void nueva_generacion(char control, int nHilos, int nHilos2){
	char ver = 0;
	FILE *fd = stdout;
	while((juego->generaciones > 0) || control == 0){
				
		int fil=juego->filas;
		int col=juego->columnas;
		int tiempo=juego->tiempo_sleep;
		char **matriz=juego->tablero;
		
		estad->vivo = 0;
		estad->muerto = 0;
		aliveA = 0;
		vivo2 = 0;
		
	
		if(matriznueva==NULL){
			return;
		}
		for(int i=0;i<fil;i++){
			if(matriznueva[i]==NULL){
				return;
			}
			memcpy(matriznueva[i],matriz[i],sizeof(char)*col);
		}
	

		for(int i = 0; i < nHilos; i++){
                	sem_post(&worker[i]);
		}

		for(int i = 0; i < nHilos; i++){
               		sem_wait(&principal[i]);
		}
	
		estad->genControl++;
		usleep(tiempo);
	
		vivo2 = aliveA + estad->vivo - estad->muerto;
	
		estad->muertocontador+=estad->muerto;
		estad->vivascontador+=estad->vivo;
		estad->acumuladorvivos+=vivo2;
		estad->acumuladormuertos+=estad->muerto;

		estad->vivaspromedio=(float)estad->acumuladorvivos/(float)estad->genControl;
		estad->muertaspromedio=(float)estad->muertocontador/(float)estad->genControl;
	
		for(int i=0;i<fil;i++){
			memcpy(juego->tablero[i],matriznueva[i],sizeof(char)*col);
		}


		if(juego->generaciones == 1 && control == 1){
			for(int i = 0; i < nHilos2; i++){
                		sem_post(&worker2[i]);
			}

			for(int i = 0; i < nHilos2; i++){
               			sem_wait(&principal2[i]);
			}
			ver = 1;
			fd = fopen("resultados.txt","wa");

		}

		if(mostrar==1)
			dibujar_grilla(matriznueva,fil, col,fd);

		mostrarEstadistica(fd);
		if(ver == 1){
			mostrarFormas(fd);
			fclose(fd);
		}

		if(juego->generaciones>0){
			juego->generaciones--;
		}
	}
	
}










