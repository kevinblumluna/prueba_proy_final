#ifndef UTIL_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#define UTIL_H



void mostrarEstadistica(FILE *fd);
/*
Funcion que dibuja una grilla en la terminal.
Recibe un arreglo 2D de chars. Si el elemento es el
numero 0, se muestra un espacio vacio, si es 1, se
muestra una x.

Cada llamada de esta funcion borrara todo lo mostrado
en pantalla.

*/
void dibujar_grilla(char **matriz, int fil, int col, FILE *fd);


/*
Llena matriz con cantidad de 1s en posiciones al azar,
el resto lo llena de 0s.
*/
void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad);

//void encontrar_vecino(char **grilla, int fil, int col);


void nueva_generacion(char control, int nHilos, int nHilos2);

//hilo trabajador
void * trabajador(void *args);

int encontrar_vecino2(juego_de_vida *juego, int posX, int posY);

void buildBloque(bloque_t *bloque);
int checkPointForm(int x, int y,char **matriz, bloque_t *bloque);
void* hiloCheckForm(void *varg);
void mostrarFormas(FILE *fd);


#endif
